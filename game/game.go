package game

import (
	"fmt"
	"io"
	"math"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/davis.bickford/gordle/matcher"
	"gitlab.com/davis.bickford/gordle/solver"
)

var randGen *rand.Rand

const (
	WordLength   = 5
	wordListPath = "wordlists/solutions.txt"
	wordListURL  = "https://static.nytimes.com/newsgraphics/2022/01/25/wordle-solver/assets/solutions.txt"
)

func downloadTextFile(filePath, url string) ([]string, error) {
	// Check if file exists at local path
	if _, err := os.Stat(filePath); err == nil {
		// File exists, read it into a slice of strings
		fileContents, err := os.ReadFile(filePath)
		if err != nil {
			return nil, err
		}

		// Convert the byte slice into a slice of strings
		lines := strings.Split(string(fileContents), "\n")

		return lines, nil
	}

	// File does not exist, download it from URL
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	// Read the file contents into a byte slice
	fileContents, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	// Create any necessary directories in the path
	dir := filepath.Dir(filePath)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return nil, err
	}

	// Create the local file and write the contents to it
	err = os.WriteFile(filePath, fileContents, 0644)
	if err != nil {
		return nil, err
	}

	// Convert the byte slice into a slice of strings
	lines := strings.Split(string(fileContents), "\n")

	return lines, nil
}

func Play(maxTries int) error {
	randGen = rand.New(rand.NewSource(time.Now().UnixNano()))
	words, err := downloadTextFile(wordListPath, wordListURL)
	if err != nil {
		return fmt.Errorf("play failed to load word list: %v", err)
	}
	word := selectRandomWord(words)
	if err != nil {
		return fmt.Errorf("play failed: %v", err)
	}

	fmt.Println("Guess a 5-letter word. You have 10 tries.")

	for i := 1; i <= maxTries; i++ {
		fmt.Printf("Guess %d: ", i)
		guess := readGuess()
		exactMatches, partialMatches, err := matcher.CountMatches(guess, word)
		if err != nil {
			fmt.Printf("bad guess: %v\n", err)
			i--
			continue
		}
		if len(exactMatches) == WordLength {
			fmt.Printf("You win! The word was %s.\n", word)
			return nil
		}
		PrintGuess(guess, exactMatches, partialMatches)
	}

	fmt.Printf("You lose. The word was %s.\n", word)

	return nil
}

func PlaySelf(doEcho bool) (int, error) {
	randGen = rand.New(rand.NewSource(time.Now().UnixNano()))
	words, err := downloadTextFile(wordListPath, wordListURL)
	word := selectRandomWord(words)
	if err != nil {
		return 0, fmt.Errorf("playself failed: %v", err)
	}

	if doEcho {
		fmt.Println("Starting game...")
	}

	i := 1
	for {
		guess := solver.ValidGuess(words)
		exactMatches, partialMatches, err := matcher.CountMatches(guess, word)
		if err != nil {
			return 0, fmt.Errorf("bad guess: %v\n", err)
		}
		if len(exactMatches) == WordLength {
			if doEcho {
				fmt.Printf("won in %v guesses - word was %s.\n", i, word)
			}
			return i, nil
		}

		if doEcho {
			PrintGuess(guess, exactMatches, partialMatches)
		}

		words, err = solver.FilterWords(words, guess, exactMatches, partialMatches)
		if err != nil {
			return 0, fmt.Errorf("playself failed: %v", err)
		}

		i++
	}
}

func selectRandomWord(words []string) string {
	return words[randGen.Intn(len(words))]
}

func readGuess() string {
	var guess string
	fmt.Scanln(&guess)
	guess = strings.TrimSpace(strings.ToLower(guess))
	if len(guess) != WordLength {
		fmt.Printf("Guesses must be %d letters long. Try again.\n", WordLength)
		return readGuess()
	}
	return guess
}

func PrintGuess(guess string, exactMatches, partialMatches map[int]struct{}) {
	fmt.Print("  ")
	for i := 0; i < len(guess); i++ {
		char := string(guess[i])
		if _, ok := exactMatches[i]; ok {
			fmt.Printf("\033[32m%s\033[0m", char) // Green
		} else if _, ok := partialMatches[i]; ok {
			fmt.Printf("\033[33m%s\033[0m", char) // Yellow
		} else {
			fmt.Print(char)
		}
	}
	fmt.Println()
}

func MonteCarlo(playCount int) (float64, int, int, error) {
	var m sync.Mutex
	var wg sync.WaitGroup

	lowest, highest := int(math.Inf(1)), 0
	scores := make([]int, 0)
	for i := 0; i < playCount; i++ {
		wg.Add(1)
		go func() {
			score, _ := PlaySelf(false)

			m.Lock()
			defer m.Unlock()
			scores = append(scores, score)

			if score < lowest {
				lowest = score
			}

			if score > highest {
				highest = score
			}

			wg.Done()
		}()
	}

	wg.Wait()
	return average(scores), lowest, highest, nil
}

func average(numbers []int) float64 {
	sum := 0
	for _, num := range numbers {
		sum += num
	}
	return float64(sum) / float64(len(numbers))
}
