package solver

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFilterWords(t *testing.T) {
	remainingWords := []string{"apple", "hello", "halve", "heave", "hedge"}
	guess := "house"
	exactMatches := map[int]struct{}{0: {}, 4: {}}
	partialMatches := map[int]struct{}{}

	expectedWords := []string{"halve", "heave", "hedge"}

	possibleWords, err := FilterWords(remainingWords, guess, exactMatches, partialMatches)
	assert.NoError(t, err)
	assert.Equal(t, expectedWords, possibleWords)
}

func TestValidGuess(t *testing.T) {
	slice := []string{"apple", "orange", "banana", "pear", "grape", "watermelon", "kiwi", "mango", "pineapple", "peach"}

	for i := 0; i < 100; i++ {
		guess := ValidGuess(slice)
		assert.Contains(t, slice, guess)
	}
}
