package solver

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/davis.bickford/gordle/matcher"
	"gitlab.com/davis.bickford/gordle/util"
)

func FilterWords(remainingWords []string, guess string, exactMatches, partialMatches map[int]struct{}) ([]string, error) {
	var possibleWords []string
	for _, word := range remainingWords {
		wordExactMatches, wordPartialMatches, err := matcher.CountMatches(guess, word)
		if err != nil {
			return nil, fmt.Errorf("filter failed: %v", err)
		}
		if util.SetsEqual(exactMatches, wordExactMatches) && util.SetsEqual(partialMatches, wordPartialMatches) {
			possibleWords = append(possibleWords, word)
		}
	}
	return possibleWords, nil
}

func ValidGuess(slice []string) string {
	randGen := rand.New(rand.NewSource(time.Now().UnixNano()))
	return slice[randGen.Intn(len(slice))]
}
