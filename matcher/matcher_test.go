package matcher

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCountMatches(t *testing.T) {
	testCases := []struct {
		name                   string
		guess, word            string
		wantExact, wantPartial map[int]struct{}
		wantError              error
	}{
		{
			name:        "guess and word have different lengths",
			guess:       "hello",
			word:        "worlds",
			wantExact:   nil,
			wantPartial: nil,
			wantError:   lengthErr,
		},
		{
			name:        "one exact match, one partial match",
			guess:       "hause",
			word:        "hello",
			wantExact:   map[int]struct{}{0: {}},
			wantPartial: map[int]struct{}{4: {}},
			wantError:   nil,
		},
		{
			name:        "one exact match, two partial matches",
			guess:       "hello",
			word:        "foley",
			wantExact:   map[int]struct{}{2: {}},
			wantPartial: map[int]struct{}{1: {}, 4: {}},
			wantError:   nil,
		},
		{
			name:        "all exact matches",
			guess:       "hello",
			word:        "hello",
			wantExact:   map[int]struct{}{0: {}, 1: {}, 2: {}, 3: {}, 4: {}},
			wantPartial: map[int]struct{}{},
			wantError:   nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotExact, gotPartial, err := CountMatches(tc.guess, tc.word)
			assert.Equal(t, tc.wantError, err)
			assert.Equal(t, tc.wantExact, gotExact)
			assert.Equal(t, tc.wantPartial, gotPartial)
		})
	}
}
