package matcher

import (
	"errors"
	"strings"
)

var (
	lengthErr = errors.New("guess and word are not equal length")
)

// CountMatches counts the exact and partial matches between two strings.
// It returns two maps of integers to empty structs, representing the
// indices of the exact and partial matches respectively, as well as an
// error if the input strings are not of equal length.
func CountMatches(guess, word string) (map[int]struct{}, map[int]struct{}, error) {
	if len(guess) != len(word) {
		return nil, nil, lengthErr
	}
	exactMatches := make(map[int]struct{})
	partialMatches := make(map[int]struct{})
	noMatches := make(map[int]struct{})
	remainingWord := ""
	for i := 0; i < len(word); i++ {
		if guess[i] == word[i] {
			exactMatches[i] = struct{}{}
		} else {
			remainingWord += string(word[i])
		}
	}

	// only count partials as many times as they occur in the word, e.g. we don't want a letter showing yellow several times if it only occurs once in the word
	for i := 0; i < len(guess); i++ {
		if _, ok := exactMatches[i]; ok {
			continue
		}
		index := strings.Index(remainingWord, string(guess[i]))
		if index >= 0 {
			partialMatches[i] = struct{}{}
			remainingWord = remainingWord[:index] + remainingWord[index+1:]
		} else {
			noMatches[i] = struct{}{}
		}
	}

	return exactMatches, partialMatches, nil
}
