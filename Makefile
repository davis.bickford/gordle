.PHONY: .mods
.mods:
	go mod download

test: .mods
	go test ./...