package main

import (
	"fmt"
	"gitlab.com/davis.bickford/gordle/game"
	"time"
)

type Mode int

const (
	Play Mode = iota
	PlaySelf
	MonteCarlo
)

const (
	maxTries            = 10
	playMode            = Play
	monteCarloPlayCount = 10000
)

func main() {
	start := time.Now()

	switch playMode {
	case Play:
		err := game.Play(maxTries)
		if err != nil {
			fmt.Println(err)
			break
		}
	case PlaySelf:
		score, err := game.PlaySelf(false)
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Printf("play self score: %v", score)
	case MonteCarlo:
		fmt.Printf("monte carlo playing %v games\n", monteCarloPlayCount)
		avg, low, high, err := game.MonteCarlo(monteCarloPlayCount)
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Printf("avg: %v, low: %v, high: %v\n", avg, low, high)
	}

	elapsed := time.Since(start)
	fmt.Printf("\nTime taken: %s\n", elapsed)
}
